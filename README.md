# README #

This service requires Java8 - docker

### How do I get set up? ###
* Install the latest JDK8.
* Clone this repo.

`./gradlew clean build` // builds the project

`./gradlew copyApp` // copy the Jar, config files to docker

`./gradlew startDockerEnvironment` // starts MySQL, phpmyadmin and the locations-service app in Docker containers

`./gradlew stopDockerEnvironment` // stops all containers

### Who do I talk to? ###
* Jbour