package com.freightos.flux.business.service;

import com.freightos.flux.api.resources.LocationsResource;
import com.freightos.flux.data.Country;
import com.freightos.flux.data.State;
import com.freightos.flux.data.Type;
import com.freightos.flux.model.Location;
import com.freightos.flux.model.Locations;
import io.dropwizard.testing.junit.ResourceTestRule;
import org.junit.After;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.*;


@RunWith(MockitoJUnitRunner.class)
public class LocationsResourceTest {

  public static final String LOCATIONS_ENDPOINT = "api/v1/locations";
  private static final LocationsService locationsService = mock(LocationsService.class);
  @ClassRule
  public static final ResourceTestRule resources =
      ResourceTestRule.builder().addResource(new LocationsResource(locationsService)).build();

  private List<Location> mockedLocations;
  private Locations locationsList;

  @Before
  public void setUp() {
    // DB init
    mockedLocations =
        Arrays.asList(
            new Location(
                "#ED7",
                Country.US,
                State.ARIZONA,
                Type.AMZ,
                "Arizona location1",
                "#ED7 - Arizona location1"),
            new Location(
                "#ED6",
                Country.US,
                State.ARIZONA,
                Type.AMZ,
                "Arizona location2",
                "#ED7 - Arizona location2"));

    locationsList = new Locations(mockedLocations);

    when(locationsService.getLocations()).thenReturn(locationsList);
  }

  @After
  public void tearDown() {
    reset(locationsService);
  }

  @Test
  public void testGetLocations() {
    Locations locationsList = resources.target(LOCATIONS_ENDPOINT).request().get(Locations.class);
    assertThat(locationsList.getLocations().size()).isEqualTo(2);

    assertThat(locationsList.getLocations().get(0).getEcommerceKey())
        .isEqualTo(mockedLocations.get(0).getEcommerceKey());

    assertThat(locationsList.getLocations().get(0).getValue())
        .isEqualTo(mockedLocations.get(0).getValue());

    assertThat(locationsList.getLocations().get(0).getLabel())
        .isEqualTo(mockedLocations.get(0).getLabel());

    verify(locationsService).getLocations();
  }

  @Test
  public void testAddLocations(){
      final Response response = resources.target(LOCATIONS_ENDPOINT).request().post(Entity.entity(locationsList, MediaType.APPLICATION_JSON_TYPE), Response.class);
    assertThat(response.getStatusInfo()).isEqualTo(Response.Status.OK);
  }
}
