Feature: Get locations

  Scenario Outline: Get Locations list
    When We send a list of <size> locations to the service,with ecommerceKey <ecommerceKey> at index <index>
    Then We expect to receive list of <size> locations, with key <ecommerceKey> at index <index>
    Examples:
      | size | ecommerceKey | index |
      | 2    | "#ED7"       | 0     |
      | 2    | "#ED6"       | 1     |

