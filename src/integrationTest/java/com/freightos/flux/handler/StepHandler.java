package com.freightos.flux.handler;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.freightos.flux.TestLocations;
import com.freightos.flux.data.Country;
import com.freightos.flux.data.State;
import com.freightos.flux.data.Type;
import com.freightos.flux.model.Location;
import com.freightos.flux.model.Locations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.junit.Assert.assertEquals;

public class StepHandler {
    private static final Logger LOGGER = LoggerFactory.getLogger(StepHandler.class);
    private static final String LOCATIONS_ENDPOINT = "http://localhost:8080/api/v1/locations";
    private final TestLocations testLocations;
    private final ObjectMapper mapper;

    public StepHandler(TestLocations testLocations) {
        this.testLocations = testLocations;
        this.mapper = new ObjectMapper();
    }

    public void postLocations() {

        List<Location> mockedLocations = Arrays.asList(
                new Location("#ED7", Country.US, State.ARIZONA, Type.AMZ, "Arizona location1", "#ED7 - Arizona location1"),
                new Location("#ED6", Country.US, State.ARIZONA, Type.AMZ, "Arizona location2", "#ED7 - Arizona location2"));

        Locations locationsList = new Locations(mockedLocations);

        io.restassured.response.Response response = given().contentType("application/json").when().body(locationsList).post(LOCATIONS_ENDPOINT);
        assertEquals(response.getStatusCode(), 200);
    }

    public void getLocations(Integer locationsListSize , String ecommerceKey, Integer index) {
        final String jsonResponse = given().when().get(LOCATIONS_ENDPOINT).body().print();

        final Locations locations;
        try {
            locations = this.mapper.readValue(jsonResponse, new TypeReference<Locations>() {
            });
            this.testLocations.setLocations(locations);

            assertEquals(
                    "Receive list of same size!",
                    locationsListSize.intValue(),
                    this.testLocations.getLocations().getLocations().size()
            );

            assertEquals("Got ecommerceKey same as "+ecommerceKey+" at index : "+index.intValue() , ecommerceKey , this.testLocations.getLocations().getLocations().get(index.intValue()).getEcommerceKey());
        } catch (IOException e) {
            LOGGER.info("Exception in running integration test", e);
        }
    }

    // ToDO: check for GET schemas

}
