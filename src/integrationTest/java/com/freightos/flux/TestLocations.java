package com.freightos.flux;

import com.freightos.flux.model.Locations;

public class TestLocations {

    private Locations locations;

    public Locations getLocations() {
        return locations;
    }

    public void setLocations(Locations locations) {
        this.locations = locations;
    }
}
