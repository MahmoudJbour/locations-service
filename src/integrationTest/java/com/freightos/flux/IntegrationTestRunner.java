package com.freightos.flux;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import io.dropwizard.testing.junit.DropwizardAppRule;
import org.junit.ClassRule;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = {
                "pretty",
                "html:build/cucumber/cucumberResults.html",
                "json:build/cucumber/cucumberResults.json"
        },
        features = "src/integrationTest/resources/features",
        glue = "", // specify extra steps
        tags = {"~@ignored"}, // specify tags to be run here
        snippets = SnippetType.CAMELCASE
)
public class IntegrationTestRunner {
}
