package com.freightos.flux;
import com.freightos.flux.handler.StepHandler;
import cucumber.api.PendingException;
import cucumber.api.java8.En;


public class Steps implements En {

    public Steps() {
        final TestLocations testWorld = new TestLocations();
        final StepHandler stepHandler = new StepHandler(testWorld);

        When("^We send a list of (\\d+) locations to the service,with ecommerceKey \"([^\"]*)\" at index (\\d+)$", (Integer size,String ecommerceKey, Integer index) -> {
            stepHandler.postLocations();
        });

        Then("^We expect to receive list of (\\d+) locations, with key \"([^\"]*)\" at index (\\d+)$", (Integer size, String ecommerceKey, Integer index) -> {
            stepHandler.getLocations(size , ecommerceKey ,index);
        });


    }
}