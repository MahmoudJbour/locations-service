package com.freightos.flux;

import com.freightos.flux.api.resources.LocationsResource;
import com.freightos.flux.api.resources.MainResource;
import com.freightos.flux.api.resources.TestLocations;
import com.freightos.flux.business.service.LocationsService;
import com.freightos.flux.core.CORSFilter;
import com.freightos.flux.core.FreightosException;
import com.freightos.flux.core.FreightosExceptionMapper;
import com.freightos.flux.db.LocationsDao;
import io.dropwizard.Application;
import io.dropwizard.jersey.jackson.JsonProcessingExceptionMapper;
import io.dropwizard.setup.Environment;
import org.skife.jdbi.v2.DBI;

import javax.sql.DataSource;
import javax.ws.rs.core.Response;

public class LocationsApplication extends Application<LocationsConfiguration> {

    public static final String SQL = "SQL";

    @Override
    public void run(final LocationsConfiguration config, final Environment environment){
    try{
        final DataSource dataSource = config.getDataSourceFactory().build(environment.metrics(), SQL);
        final DBI dbi = new DBI(dataSource);

        final LocationsDao locationsDao = dbi.onDemand(LocationsDao.class);

        final LocationsService locationsService = new LocationsService(locationsDao) ;
        final LocationsResource locationsResource = new LocationsResource(locationsService);
        environment.jersey().register(new JsonProcessingExceptionMapper(true));
        environment.jersey().register(locationsResource);
        environment.jersey().register(new FreightosExceptionMapper());
        environment.jersey().register(new MainResource());
        environment.jersey().register(new CORSFilter());
        environment.jersey().register(new TestLocations(locationsService));
    }catch (Exception e){
        throw new FreightosException(Response.Status.SERVICE_UNAVAILABLE.getStatusCode(),"Unable to boot up the application");
    }

    }

    public static void main(String... args) throws Exception {
        new LocationsApplication().run(args);
    }
}
