package com.freightos.flux.business.service;

import com.freightos.flux.core.FreightosException;
import com.freightos.flux.db.LocationsDao;
import com.freightos.flux.model.Location;
import com.freightos.flux.model.Locations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.core.Response;
import java.sql.Timestamp;
import java.util.List;


public class LocationsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationsService.class);
    private final LocationsDao locationsDao;

    public LocationsService(LocationsDao locationsDao){
        this.locationsDao = locationsDao;
    }


    /**
     * Get all locations as a list of location model
     *
     * @return List<Location>
     */
    public Locations getLocations() {
        try {
            List<Location> locations = locationsDao.getLocations();
            return new Locations(locations);
        } catch (FreightosException e) {
            throw new FreightosException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), "An exception occur while trying to retrieve locations list");
        }
    }

    /**
     * Passed a list of locations, loop thought each one and add them to the table.
     *
     * @param locations
     */
    public void addLocations(final Locations locations) {
        try {
            emptyTable();
            locationsDao.resetTableAutoIncrement();
            for (int i = 0; i < locations.getLocations().size(); i++) {
                locations.getLocations().get(i).setCreatedOn(new Timestamp(System.currentTimeMillis()));
                locations.getLocations().get(i).setModifiedOn(new Timestamp(System.currentTimeMillis()));
                locationsDao.addLocation(locations.getLocations().get(i));
            }
            LOGGER.info("All locations has been added.");
        } catch (FreightosException e) {
            throw new FreightosException(Response.Status.BAD_REQUEST.getStatusCode(), "An exception occur while trying to add locations list!");
        }
    }

    /**
     * Empties locations table, called before adding any new list
     */
    public void emptyTable() {
        try {
            locationsDao.emptyTable();
            LOGGER.info("All locations has been removed.");
        } catch (FreightosException e) {
            throw new FreightosException(Response.Status.BAD_REQUEST.getStatusCode(), "An exception occur while trying to empties locations list!");
        }
    }
}
