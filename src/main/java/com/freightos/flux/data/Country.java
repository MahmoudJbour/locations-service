package com.freightos.flux.data;

public enum Country {
  US("United States of America", "US"),
  GB("United Kingdom", "GB");

  private final String englishName;
  private final String countryCode;

  Country(String englishName, String countryCode) {
    this.countryCode = countryCode;
    this.englishName = englishName;
  }

  public String getEnglishName() {
    return englishName;
  }

  public String getCountryCode() {
    return countryCode;
  }
}
