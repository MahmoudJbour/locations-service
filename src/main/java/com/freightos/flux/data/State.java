package com.freightos.flux.data;

public enum State {
  ARIZONA("ARIZONA", "AZ"),
  CALIFORNIA("California", "CA"),
  COLORADO("Colorado", "CO"),
  CONNECTICUT("Connecticut", "CT"),
  DELAWARE("Delaware", "DE"),
  FLORIDA("Florida", "FL"),
  GEORGIA("Georgia", "GA"),
  ILLINOIS("Illinois", "IL"),
  INDIANA("Indiana", "IN"),
  KANSAS("Kansas", "KS"),
  KENTUCKY("Kentucky", "KY"),
  MARYLAND("Maryland", "MD"),
  MASSACHUSETTS("Massachusetts", "MA"),
  MICHIGAN("Michigan", "MI"),
  MINNESOTA("Minnesota", "MN"),
  NEVADA("Nevada", "NV"),
  NEW_HAMPSHIRE("New Hampshire", "NH"),
  NEW_JERSEY("New Jersey", "NJ"),
  NEW_YORK("New York", "NY"),
  NORTH_CAROLINA("North Carolina", "NC"),
  OHIO("Ohio", "OH"),
  OKLAHOMA("Oklahoma", "OK"),
  OREGON("Oregon", "OR"),
  PENNSYLVANIA("Pennsylvania", "PA"),
  SOUTH_CAROLINA("South Carolina", "SC"),
  TENNESSEE("Tennessee", "TN"),
  TEXAS("Texas", "TX"),
  VIRGINIA("Virginia", "VA"),
  WASHINGTON("Washington", "WA"),
  LOS_ANGELES("Los angeles", "LA"),
  WISCONSIN("Wisconsin", "WI");

  private final String name;
  private final String code;

  State(String name, String code) {
    this.name = name;
    this.code = code;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }
}
