package com.freightos.flux.data;

public enum Type {
  AMZ("Amazon address", "AMZ", "amazonAddress"),
  UPS("United Parcel Service", "UPS", "UPS");

  private final String name;
  private final String code;
  private final String typeProperty;

  Type(String name, String code, String typeProperty) {
    this.name = name;
    this.code = code;
    this.typeProperty = typeProperty;
  }

  public String getName() {
    return name;
  }

  public String getCode() {
    return code;
  }

  public String getTypeProperty() {
    return typeProperty;
  }
}
