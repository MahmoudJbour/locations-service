package com.freightos.flux.db;

import com.freightos.flux.db.utils.LocationsMapper;
import com.freightos.flux.model.Location;
import java.util.List;
import org.skife.jdbi.v2.sqlobject.BindBean;
import org.skife.jdbi.v2.sqlobject.SqlQuery;
import org.skife.jdbi.v2.sqlobject.SqlUpdate;
import org.skife.jdbi.v2.sqlobject.customizers.RegisterMapper;

@RegisterMapper(LocationsMapper.class)
public interface LocationsDao {

    String LOCATIONS_SELECT = "SELECT * FROM `locations` ORDER BY `label`";
    String LOCATIONS_INSERT = "INSERT INTO `locations` (`id`, `createdOn`, `modifiedOn`, `ecommerceKey`, `country`, `state`, `type`, `value`, `label`) " +
            "VALUES (:id, :createdOn, :modifiedOn, :ecommerceKey, :country, :state, :type, :value, :label)";
    String LOCATIONS_DELETE = "DELETE FROM `locations`";
    String LOCATIONS_RESET = "ALTER TABLE `locations` AUTO_INCREMENT = 1";


    /**
     * Get list of locations
     *
     * @return List<Location>
     */
    @SqlQuery(LOCATIONS_SELECT)
    List<Location> getLocations();

    /**
     * Add single location object
     *
     * @param location
     */
    @SqlUpdate(LOCATIONS_INSERT)
    void addLocation(@BindBean final Location location);

    /**
     * Empties locations table
     *
     * @return status
     */
    @SqlUpdate(LOCATIONS_DELETE)
    int emptyTable();

    /**
     * Reset the AI key in the DB, this called each time we re add the list of locations
     */
    @SqlUpdate(LOCATIONS_RESET)
    void resetTableAutoIncrement();
}
