package com.freightos.flux.db.utils;

import com.freightos.flux.core.FreightosException;
import com.freightos.flux.data.Country;
import com.freightos.flux.data.State;
import com.freightos.flux.data.Type;
import com.freightos.flux.model.BaseEntity;
import com.freightos.flux.model.Location;
import org.skife.jdbi.v2.StatementContext;
import org.skife.jdbi.v2.tweak.ResultSetMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.ws.rs.core.Response;
import java.sql.ResultSet;
import java.sql.SQLException;

public class LocationsMapper implements ResultSetMapper<Location> {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationsMapper.class);


    /**
     * Map results returned from DB to Location model
     *
     * @param index
     * @param r
     * @param ctx
     * @return
     * @throws SQLException
     */
    @Override
    public Location map(int index, ResultSet r, StatementContext ctx) {

        try {
            String state = r.getString(Location.STATE);
            return new Location(
                    r.getLong(BaseEntity.ID),
                    r.getString(Location.ECOMMERCE_KEY),
                    Country.valueOf(r.getString(Location.COUNTRY)),
                    state != null ? State.valueOf(state) : null,
                    Type.valueOf(r.getString(Location.TYPE)),
                    r.getString(Location.VALUE),
                    r.getString(Location.LABEL),
                    r.getTimestamp(Location.CREATED_ON),
                    r.getTimestamp(Location.MODIFIED_ON)
            );
        } catch (SQLException e) {
            throw new FreightosException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(),"Unable to get results from DB.");
        }

    }
}
