package com.freightos.flux.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.freightos.flux.business.service.LocationsService;
import com.freightos.flux.model.Location;
import com.freightos.flux.model.Locations;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

@Path("/testLocations")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")

public class TestLocations {

    private final LocationsService locationService;

    public TestLocations(final LocationsService locationService) {
        this.locationService = locationService;
    }

    @GET
    @Timed
    public Response testLocations() throws RuntimeException, IOException, InterruptedException {
        Locations locations = locationService.getLocations();

        Map<String, String> output = new HashMap<>();
        for(Location loc : locations.getLocations()){

            URL url = new URL("https://maps.googleapis.com/maps/api/place/textsearch/xml?query="+ URLEncoder.encode(loc.getValue(), "UTF-8")+"&key=AIzaSyDFwbLFer-KrdLYFTRLdMlRH4agX8h9MOo");

            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Accept", "application/json");


            Scanner scan = new Scanner(url.openStream());
            String entireResponse = new String();
            while (scan.hasNext())
                entireResponse += scan.nextLine();

            System.out.println("Response : "+entireResponse);

            if (conn.getResponseCode() != 200 && !entireResponse.contains("Ok")) {
                output.put(loc.getValue().toString() , " Has been failed!");
                throw new RuntimeException("HTTP error code : "
                + conn.getResponseCode());
            }

            scan.close();
            TimeUnit.SECONDS.sleep(10);
        }

        return Response.ok().entity(output).build();
    }

}
