package com.freightos.flux.api.resources;

import com.codahale.metrics.annotation.Timed;
import com.freightos.flux.business.service.LocationsService;
import com.freightos.flux.core.FreightosException;
import com.freightos.flux.model.Locations;
import org.skife.jdbi.v2.exceptions.UnableToObtainConnectionException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.constraints.NotNull;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;


@Path("/api/v1/locations")
@Produces(MediaType.APPLICATION_JSON + "; charset=utf-8")
@Consumes(MediaType.APPLICATION_JSON)
public class LocationsResource {

    private static final Logger LOGGER = LoggerFactory.getLogger(LocationsResource.class);
    private final LocationsService locationService;

    public LocationsResource(final LocationsService locationService) {
        this.locationService = locationService;
    }

    @GET
    @Timed
    public Locations getLocations() throws FreightosException {
        try {
            Locations response = locationService.getLocations();
            LOGGER.info("[getLocations] All locations retrieved successfully");
            return response;
        } catch (Exception e) {
            if (e instanceof UnableToObtainConnectionException) {
                throw new FreightosException(Response.Status.NOT_FOUND.getStatusCode(), "Unable to connect to the database!");
            }
            throw new FreightosException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Unable to get locations list");
        }
    }

    @POST
    @Timed
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addLocations(final @NotNull Locations locations) throws FreightosException, IOException {
        try {
            locationService.addLocations(locations);
            LOGGER.info("[createLocation] Successfully Added!");
            return Response.ok().build();
        } catch (Exception e) {
            if (e instanceof UnableToObtainConnectionException) {
                throw new FreightosException(Response.Status.NOT_FOUND.getStatusCode(), "Unable to connect to the database!");
            }
            throw new FreightosException(Response.Status.INTERNAL_SERVER_ERROR.getStatusCode(), "Unable to add locations list");
        }
    }
}

