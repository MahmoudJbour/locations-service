package com.freightos.flux.api.resources;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

@Path("/")
public class MainResource {
    @GET
    public Response healthCheck(){
        return Response.ok().build();
    }
}
