package com.freightos.flux.model;

import com.freightos.flux.data.Country;
import com.freightos.flux.data.State;
import com.freightos.flux.data.Type;

import java.sql.Timestamp;


public class Location extends BaseEntity {

    public static final String LOCATIONS = "locations";

    public static final String ECOMMERCE_KEY = "ecommerceKey";
    public static final String COUNTRY = "country";
    public static final String STATE = "state";
    public static final String TYPE = "type";
    public static final String VALUE = "value";
    public static final String LABEL = "label";
    private String ecommerceKey;
    private Country country;
    private State state;
    private Type type;
    private String value;
    private String label;


    public Location() {
    }

    public Location(String ecommerceKey, Country country, State state, Type type, String value, String label) {
        super();
        this.ecommerceKey = ecommerceKey;
        this.country = country;
        this.state = state;
        this.type = type;
        this.value = value;
        this.label = label;
    }

    public Location(Long id, String ecommerceKey, Country country, State state, Type type, String value, String label, Timestamp cd, Timestamp md) {
        super(id, cd, md);
        this.ecommerceKey = ecommerceKey;
        this.country = country;
        this.state = state;
        this.type = type;
        this.value = value;
        this.label = label;
    }

    public String getEcommerceKey() {
        return ecommerceKey;
    }

    public void setEcommerceKey(String ecommerceKey) {
        this.ecommerceKey = ecommerceKey;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public State getState() {
        return state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }

}
