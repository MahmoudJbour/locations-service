package com.freightos.flux.model;

import java.sql.Timestamp;

public class BaseEntity {

  public static final String ID = "id";
  private long id;

  public static final String CREATED_ON = "createdOn";
  private Timestamp createdOn;

  public static final String MODIFIED_ON  = "modifiedOn";
  private Timestamp modifiedOn;

  public BaseEntity() {
  }

  public BaseEntity(long id, Timestamp createdOn, Timestamp modifiedOn) {
    this.id = id;
    this.createdOn = createdOn;
    this.modifiedOn = modifiedOn;
  }

  public long getId() {
    return id;
  }

  public void setId(long id) {
    this.id = id;
  }

  public Timestamp getCreatedOn() {
    return createdOn;
  }

  public void setCreatedOn(Timestamp createdOn) {
    this.createdOn = createdOn;
  }

  public Timestamp getModifiedOn() {
    return modifiedOn;
  }

  public void setModifiedOn(Timestamp modifiedOn) {
    this.modifiedOn = modifiedOn;
  }
}
