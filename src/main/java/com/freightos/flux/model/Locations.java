package com.freightos.flux.model;

import javax.xml.bind.annotation.XmlRootElement;
import java.util.ArrayList;
import java.util.List;

/**
 * Wrapper to represent a list of <link>{@link Location}</link>
 */
@XmlRootElement
public class Locations {

    private List<Location> locations;

    public Locations(List<Location> locationsData) {
        this.locations = locationsData;
    }

    public Locations() {
        this.locations = new ArrayList<>();
    }

    public List<Location> getLocations() {
        return locations;
    }

    public void setLocations(List<Location> locations) {
        this.locations = locations;
    }
}
