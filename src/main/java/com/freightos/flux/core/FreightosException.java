package com.freightos.flux.core;

public final class FreightosException extends RuntimeException{

  private final int statusCode;
  private final String errorMessage;

  public FreightosException(final int statusCode, final String errorMessage) {
    super(errorMessage,null);
    this.statusCode = statusCode;
    this.errorMessage = errorMessage;
  }


  public int getStatusCode() {
    return statusCode;
  }

  public String getErrorMessage() {
    return errorMessage;
  }
}
