package com.freightos.flux.core;

import io.dropwizard.jersey.errors.ErrorMessage;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;

public class FreightosExceptionMapper implements ExceptionMapper<FreightosException> {

  @Override
  public Response toResponse(final FreightosException exception) {
    return Response.status(exception.getStatusCode())
        .entity(new ErrorMessage(exception.getStatusCode(), exception.getErrorMessage()))
        .build();
  }
}
